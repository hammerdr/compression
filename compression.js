exports.compress = function(datum) {
  let result = ''
  let lastCh = null
  let lastChCount = 0

  datum.split('').forEach(function(ch) {
    if (ch === lastCh) {
      lastChCount++
    } else if (lastCh === null) {
      lastCh = ch
      lastChCount = 1
    } else {
      result += lastChCount + lastCh
      lastCh = ch
      lastChCount = 1
    }
  })

  return result + lastChCount + lastCh
}
